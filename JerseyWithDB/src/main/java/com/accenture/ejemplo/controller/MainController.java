package com.accenture.ejemplo.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.accenture.ejemplo.model.Banco;
import com.accenture.ejemplo.model.Empleado;
import com.accenture.ejemplo.model.Empresa;
import com.accenture.ejemplo.model.InputEmpleado;
import com.accenture.ejemplo.model.InputEmpresa;
import com.accenture.ejemplo.repository.BancoDao;
import com.accenture.ejemplo.repository.EmpleadoDao;
import com.accenture.ejemplo.repository.EmpresaDao;

@Path("/gobierno")
public class MainController {
	
	@Autowired
	private BancoDao daobanco;
	
	@Autowired
	private EmpleadoDao daoempleado;
	
	@Autowired
	private EmpresaDao daoempresa;
	
	
	
	//1) Dar de alta un banco.
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/banco")
	public Banco createBanco(Banco banco) {
		System.out.println("Ingresado nuevo banco.");
		return daobanco.save(banco);
	}
	
	//2) Dar de alta una empresa y asignarle un banco en el momento del alta (pasarle ID pk del banco).
	@POST
	//Sin las sig. anotacion funciona igual, pero no recibe como JSON
	// el postman, recibe como String.
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empresa")
	public Response createEmpresa(InputEmpresa ingreso){
		
		Banco bn = daobanco.findById(ingreso.getIdBanco()).orElse(null);
		Empresa auxEmpresa = new Empresa();
		JSONObject obj = new JSONObject();
		
		if(bn!=null) {
			auxEmpresa.setBanco(bn);
			auxEmpresa.setNombre(ingreso.getEmpresa().getNombre());
			auxEmpresa.setDireccion(ingreso.getEmpresa().getDireccion());
			auxEmpresa.setLocalidad(ingreso.getEmpresa().getLocalidad());
			auxEmpresa.setDescripcion(ingreso.getEmpresa().getDescripcion());
			
			Empresa aux = daoempresa.save(auxEmpresa);
			obj.put("error", 0);
			obj.put("message", "Creada nueva empresa con ID: "+aux.getId());
			
			return Response.ok(obj.toString()).build();		
		}else {
			obj.put("error", 1);
			obj.put("message", "Banco not found.");
			
			return Response.ok(obj.toString()).build();
		}
	}
	
	//3) Dar de alta un empleado asignandole una empresa (recibe pk de la empresa).
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empleado")
	public Response createEmpleado(InputEmpleado ingreso) {
		
		Empresa emp = daoempresa.findById(ingreso.getIdEmpresa()).orElse(null);
		Empleado auxEmpleado = new Empleado();
		JSONObject obj = new JSONObject();
		
		if(emp!=null) {
			
			auxEmpleado.setEmpresa(emp);
			auxEmpleado.setNombre(ingreso.getEmpleado().getNombre());
			auxEmpleado.setApellido(ingreso.getEmpleado().getApellido());
			auxEmpleado.setDireccion(ingreso.getEmpleado().getDireccion());
			auxEmpleado.setDni(ingreso.getEmpleado().getDni());
			
			Empleado aux = daoempleado.save(auxEmpleado);	
			obj.put("error", 0);
			obj.put("message", "Se creo la empresa con ID: "+aux.getId());			
			
			return Response.ok(obj.toString()).build();
		}else {
			obj.put("error", 1);
			obj.put("message", "Empresa not found.");
			
			return Response.ok(obj.toString()).build();
		}
	}
	
	//4) Los empleados se pueden mudar, la API recibe dni y direccion nueva
	@PUT	
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empleado")
	public Response mudarse(InputMudanza ingreso) {
		
		Empleado empEncontrado = daoempleado.findByDni(ingreso.getDni());
		JSONObject obj = new JSONObject();
		
		if(empEncontrado!=null) {
			empEncontrado.setDireccion(ingreso.getNuevaDireccion());
			daoempleado.save(empEncontrado);
			obj.put("error", 0);
			obj.put("message", "El domicilio fue cambiado con exito.");
			
			return Response.ok(obj.toString()).build();
		}else {
			
			obj.put("error", 1);
			obj.put("message", "No se encontro un empleado con ese DNI");
			
			return Response.ok(obj.toString()).build();				
		}

	}
	
	//5) Se requiere un listado de todos los empleados de una empresa , la API
	// recibe el id de pk de la empresa, y la salida debe tener los datos de
	// la empresa con el listado de empleados. ID por URL.
	// #Tengo que devolver string pq response no devuelve json arrays.
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/empleados/{id_empresa}")
	public String listarEmpleados(@PathParam("id_empresa") long idRecibido) {
		
		Empresa empresaBuscada = daoempresa.findById(idRecibido).orElse(null);
		List<Empleado> empFound = daoempleado.findByEmpresa(empresaBuscada);
		JSONObject obj = new JSONObject();
		JSONArray array = new JSONArray();
		
		if(!empFound.isEmpty()) {
			
			obj.put("nombre", empresaBuscada.getNombre());
			obj.put("direccion", empresaBuscada.getDireccion());
			obj.put("localidad", empresaBuscada.getLocalidad());
			obj.put("descripcion", empresaBuscada.getDescripcion());
			obj.put("banco", empresaBuscada.getBanco().getNombre());
			
			for(Empleado empAux: empFound) {
				JSONObject aux = new JSONObject();
				aux.put("empleado",empAux.getNombre() +" "+ empAux.getApellido());
				array.put(aux);	
			}
			
			JSONObject last = new JSONObject();
			last.put("Datos de la empresa", obj);
			last.put("lista de empleados", array);
			
			System.out.println(last);

			return last.toString();
		}else {
			obj.put("error", 1);
			obj.put("message", "Esa empresa no tiene empleados.");
			
			return obj.toString();
		}
	}
	
	

}
