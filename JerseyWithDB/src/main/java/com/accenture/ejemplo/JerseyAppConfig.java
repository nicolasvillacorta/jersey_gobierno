package com.accenture.ejemplo;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/app")
public class JerseyAppConfig extends ResourceConfig {
	
	public JerseyAppConfig() {
		packages("com.accenture.ejemplo.controller");
	}

}
