package com.accenture.ejemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JerseyWithDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(JerseyWithDbApplication.class, args);
	}

}
