package com.accenture.ejemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.ejemplo.model.Banco;

public interface BancoDao extends JpaRepository<Banco, Long> {

}
