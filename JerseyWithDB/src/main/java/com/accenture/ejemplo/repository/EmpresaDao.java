package com.accenture.ejemplo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.ejemplo.model.Empresa;

public interface EmpresaDao extends JpaRepository<Empresa, Long>{

}
