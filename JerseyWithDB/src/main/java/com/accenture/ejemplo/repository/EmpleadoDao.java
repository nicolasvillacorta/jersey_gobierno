package com.accenture.ejemplo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.ejemplo.model.Empleado;
import com.accenture.ejemplo.model.Empresa;

public interface EmpleadoDao extends JpaRepository<Empleado, Long> {

	public Empleado findByDni(String dni);
	
	public List<Empleado> findByEmpresa(Empresa empresa);
	
}
